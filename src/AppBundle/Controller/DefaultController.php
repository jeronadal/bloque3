<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Anuncio;
use AppBundle\Entity\Usuario;
use AppBundle\Entity\Seguimiento;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Security;
use Doctrine\ORM\Tools\Pagination\Paginator;

class DefaultController extends Controller
{
    /**
     * Lists all Anuncio entities.
     *
     * @Route("/", name="route_homepage")
     * @Template("AppBundle:anuncio:index.html.twig")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $anuncios = $em->getRepository('AppBundle:Anuncio')->findAll();
        $usuario=$this->getUser();
        $seguimientos = $em->getRepository('AppBundle:Seguimiento')->findByUsuarioId($usuario);

        return array(
            'anuncios' => $anuncios,
            'categorias'=> $categorias,
            'seguimientos'=> $seguimientos,
        );
    }

    /**
     * Lists all Anuncio entities.
     *
     * @Route("/mapa", name="route_mapa")
     * @Template("AppBundle:default:mapa.html.twig")
     * @Method("GET")
     */
    public function mapAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $anuncios = $em->getRepository('AppBundle:Anuncio')->findAll();

        return array(
            'anuncios' => $anuncios,
            'categorias'=> $categorias,
        );
    }

    /**
     * @Route("/login", name="route_login")
     * @Template("AppBundle:default:login.html.twig")
     */
    public function loginAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();

        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return array(
                // last username entered by the user
                'categorias'=> $categorias,
                'last_username' => $lastUsername,
                'error'         => $error,
        );
    }

    /**
     * @Route("/listar", name="route_listar")
     * @Template("AppBundle:default:listar.html.twig")
     */
    public function listarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $anuncios = $em->getRepository('AppBundle:Anuncio')->findAll();
        $usuario=$this->getUser();
        $anunciosUsuario = $em->getRepository('AppBundle:Anuncio')->findByUsuario($usuario);
        $seguimientosUsuario = $em->getRepository('AppBundle:Seguimiento')->findByUsuarioId($usuario);

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            return array(
                'anuncios' => $anuncios,
                'categorias' => $categorias,
                'seguimientosUsuario'=>$seguimientosUsuario,
            );
        }
        else
        {
            return array(
                'anuncios' => $anunciosUsuario,
                'categorias' => $categorias,
                'seguimientosUsuario'=>$seguimientosUsuario,
            );
        }
    }


    /**
     * @Route("/buscarListar", name="route_buscador_listar")
     * @Template("AppBundle:default:listar.html.twig")
     * @Method({"POST"})
     */
    public function buscadorlistarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $usuario=$this->getUser();
        $seguimientosUsuario = $em->getRepository('AppBundle:Seguimiento')->findByUsuarioId($usuario);

        $texto = $request->request->get('texto');
        $radio = $_POST['optionsRadios'];

        if ($radio === "todas")
        {
            $anuncios = $em->getRepository('AppBundle:Anuncio')->buscarTodas($texto,$usuario);
        }
        else
        {
            $anuncios = $em->getRepository('AppBundle:Anuncio')->buscarAlguna($texto,$usuario);
        }


        return array(
            // last username entered by the user
            'categorias'=> $categorias,
            'anuncios'=> $anuncios,
            'seguimientosUsuario'=>$seguimientosUsuario,
        );
    }

    public function sacarFecha($fecha)
    {
        $date= date('Y-m-j');

        if($_POST['fecha']==="hoy"){$fecha=$date;}

        if($_POST['fecha']==="ayer")
        {
            $fecha=strtotime ( '-1 day' , strtotime ( $date ) ) ;
            $fecha = date ( 'Y-m-j' , $fecha );
        }

        if($_POST['fecha']==="ultimaSemana")
        {
            $fecha=strtotime('-7 day',strtotime($date));
            $fecha = date('Y-m-j' ,$fecha);
        }
        if($_POST['fecha']==="ultimoMes")
        {
            $fecha=strtotime('-30 day',strtotime($date));
            $fecha = date('Y-m-j' ,$fecha);
        }
        if($_POST['fecha']==="todo")
        {
            $fecha = '0000-00-00';
        }
        return $fecha;
    }

    /**
     * @Route("/buscar", name="route_buscador")
     * @Template("AppBundle:anuncio:index.html.twig")
     * @Method({"POST"})
     */
    public function buscadorAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $usuario=$this->getUser();
        $seguimientos = $em->getRepository('AppBundle:Seguimiento')->findByUsuarioId($usuario);

        $texto = $request->request->get('texto');
        $valor = $request->request->get('fecha');
        $fecha = $this->sacarFecha($valor);
        $precioMinimo = $request->request->get('precioMinimo');
        $precioMaximo = $request->request->get('precioMaximo');

        $anuncios = $em->getRepository('AppBundle:Anuncio')->buscar($texto,$fecha,$precioMinimo, $precioMaximo);


        return array(
            // last username entered by the user
            'categorias'=> $categorias,
            'anuncios'=> $anuncios,
            'seguimientos'=> $seguimientos,
        );
    }

    /**
     * @Route("multimedia/video", name="route_video")
     * @Template("AppBundle:default:video.html.twig")
     */
    public function videoAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        return array(
            // last username entered by the user
            'categorias'=> $categorias,
        );
    }
    /**
     * @Route("multimedia/galeria", name="route_galeria")
     * @Template("AppBundle:default:galeria.html.twig")
     */
    public function galeriaAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $anuncios = $em->getRepository('AppBundle:Anuncio')->findAll();


        return array(
            'anuncios' => $anuncios,
            'categorias'=> $categorias,
        );
    }
    /**
     * @Route("multimedia/slideshow", name="route_slideshow")
     * @Template("AppBundle:default:slideshow.html.twig")
     */
    public function slideshowAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $anuncios = $em->getRepository('AppBundle:Anuncio')->findAll();


        return array(
            'anuncios' => $anuncios,
            'categorias'=> $categorias,
        );
    }

    /**
     * @Route("multimedia/puzzle", name="route_puzzle")
     * @Template("AppBundle:default:puzzle.html.twig")
     */
    public function puzzleAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $anuncios = $em->getRepository('AppBundle:Anuncio')->findAll();


        return array(
            'anuncios' => $anuncios,
            'categorias'=> $categorias,
        );
    }
}
