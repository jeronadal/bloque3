<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Usuario;
use AppBundle\Form\UsuarioType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use AppBundle\Form\CambiarPassword;


/**
 * Usuario controller.
 *
 * @Route("/usuario")
 */
class UsuarioController extends Controller
{
    /**
     * Lists all Usuario entities.
     *
     * @Route("/", name="usuario_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $usuarios = $em->getRepository('AppBundle:Usuario')->findAll();

        return $this->render('usuario/index.html.twig', array(
            'usuarios' => $usuarios,
        ));
    }

    /**
     * @Route("/new", name="usuario_new")
     * @Template("AppBundle:usuario:new.html.twig")
     * @Method({"GET"})
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $form = $this->createForm('AppBundle\Form\UsuarioType');

        return array(
            'categorias' => $categorias,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/registro", name="registro")
     * @Method({"GET", "POST"})
     */
    public function registro(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();

        $usuario = new Usuario();
        $form = $this->createForm('AppBundle\Form\UsuarioType', $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $usuario->setIsAdmin(0);
            $usuario->setIsActive(1);
            $password = $this->container->get('security.password_encoder')->encodePassword($usuario, $usuario->getPassword());
            $usuario->setPassword($password);
            $usuario->setImagen('imagenes/anonim.png');


            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();

            if(!$this->getUser())
            {
                $token = new UsernamePasswordToken($usuario, null, 'dwes_symfony_area_protegida', $usuario->getRoles());
                $this->get('security.context')->setToken($token);
                $this->get('session')->set('_security_main', serialize($token));
            }

            return $this->redirectToRoute('route_listar');
        }
        else
        {
            return $this->render('AppBundle:usuario:new.html.twig', array(
                'categorias' => $categorias,
                'usuario' => $usuario,
                'form' => $form->createView(),
            ));
        }
    }

    /**
        *
        * @Route("/{id}/listar/usuario", name="usuario_listar")
        * @Method("GET")
       */

   public function listarUsuario(Usuario $usuario)
   {
        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();

        if ($user->getId()==$usuario->getId()||$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            return $this->render('AppBundle:default:listarUsuario.html.twig', array(
               'usuario' => $usuario,
                'categorias' => $categorias,
            ));
        }
        else
        {
            $this->addFlash(
                'error',
                'No puedes ver la información de otro usuario'
                );
            return $this->redirect($this->generateUrl('route_listar'));

        }

    }

    /**
     * @Route("/{id}/cambiarPassword", name="cambiar_password")
     * @Method({"GET", "POST"})
     */
    public function cambiarPassword(Request $request,Usuario $usuario)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();

        $form = $this->createForm('AppBundle\Form\CambiarPasswordType', $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $password = $this->container->get('security.password_encoder')->encodePassword($usuario, $usuario->getPassword());
            $usuario->setPassword($password);
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();

            $this->addFlash(
                'notice',
                'Password cambiado correctamente'
            );
            return $this->redirectToRoute('route_listar');
        }
        else
        {
            return $this->render('AppBundle:usuario:cambiarPassword.html.twig', array(
                'categorias' => $categorias,
                'usuario' => $usuario,
                'form' => $form->createView(),
            ));
        }

    }

    /**
     * @Route("/{id}/cambiarImagen", name="cambiar_imagen")
     * @Method({"GET", "POST"})
     */
    public function cambiarImagen(Request $request,Usuario $usuario)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();

        $form = $this->createForm('AppBundle\Form\CambiarImagenUsuarioType', $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

            $usuario->upload($usuario->getImagen());
            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();

            $this->addFlash(
                'notice',
                'Imagen cambiada correctamente'
            );

            return $this->redirectToRoute('route_listar');
        }
        else
        {
            return $this->render('AppBundle:usuario:cambiarImagen.html.twig', array(
                'categorias' => $categorias,
                'usuario' => $usuario,
                'form' => $form->createView(),
            ));
        }

    }

    /**
     * @Route("/{id}/eliminarImagen", name="eliminar_imagen")
     * @Method({"GET", "POST"})
     */
    public function eliminarImagen(Usuario $usuario)
    {

        $usuario->setImagen('imagenes/anonim.png');
        $em = $this->getDoctrine()->getManager();
        $em->persist($usuario);
        $em->flush();

        $this->addFlash(
            'notice',
            'Imagen eliminada correctamente'
        );

        return $this->redirectToRoute('route_listar');
    }




    /**
     * Displays a form to edit an existing Usuario entity.
     *
     * @Route("/{id}/edit", name="usuario_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Usuario $usuario)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();

        $editForm = $this->createForm('AppBundle\Form\EditarUsuarioType', $usuario);
        $editForm->handleRequest($request);

        if ($usuario==$this->getUser() || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            if ($editForm->isSubmitted() && $editForm->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                $em->persist($usuario);
                $em->flush();
                $this->addFlash(
                    'notice',
                    'El Usuario se ha editado correctamente'
                );
                return $this->redirectToRoute('route_listar');
            }

            return $this->render('AppBundle:usuario:edit.html.twig', array(
                'usuario' => $usuario,
                'edit_form' => $editForm->createView(),
                'categorias' => $categorias,
            ));
        }
        else
        {
            $this->addFlash(
                'error',
                'No puedes editar otro usuario'
            );
            return $this->redirect($this->generateUrl('route_listar'));
        }
    }

    /**
     *
     * @Route("/administrar", name="route_administrar")
     * @Method("GET")
     */
    public function administrarAction()
    {
        $em = $this->getDoctrine()->getManager();
        $usuarios = $em->getRepository('AppBundle:Usuario')->findAll();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();

        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            return $this->render('AppBundle:usuario:administrar.html.twig', array(
                'usuarios' => $usuarios,
                'categorias' => $categorias,
            ));
        }
        else
        {
            $this->addFlash(
                'error',
                'Solo el administrador puede acceder'
            );
            return $this->redirect($this->generateUrl('route_listar'));
        }
    }

    /**
     * Deletes a Anuncio entity.
     *
     * @Route("/{id}/delete", name="usuario_delete")
     * @Method({"GET"})
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $usuario = $em->getRepository('AppBundle:Usuario')->find($id);
        $ofertas = $em->getRepository('AppBundle:Oferta')->findByUsuario($id);
        $anunciosUsuario = $em->getRepository('AppBundle:Anuncio')->findByUsuario($usuario);
        $seguimientosUsuario = $em->getRepository('AppBundle:Seguimiento')->findByUsuarioId($usuario);

        if ( $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            if(count($anunciosUsuario)>0 || count($ofertas)>0 || count($seguimientosUsuario)>0)
            {
                $this->addFlash(
                    'error',
                    'No puedes borrar el usuario si tiene aununcios, ofertas o ha hecho un seguimiento. Antes debes eliminarlos.'
                );
                return $this->redirect($this->generateUrl('route_administrar'));
            }
            else
            {
                $em->remove($usuario);
                $em->flush();
                $this->addFlash(
                    'notice',
                    'El Usuario se ha eliminado correctamente'
                );
                return $this->redirect($this->generateUrl('route_administrar'));
            }
        }

        else
        {
            $this->addFlash(
                'error',
                'Solo el administrador puede acceder'
            );
            return $this->redirect($this->generateUrl('route_listar'));
        }

    }

    /**
     * @Route("/newAdmin", name="usuarioAdmin_new")
     * @Template("AppBundle:usuario:newAdmin.html.twig")
     * @Method({"GET"})
     */
    public function newAdminAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $form = $this->createForm('AppBundle\Form\UsuarioAdminType');

        return array(
            'categorias' => $categorias,
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/registroAdmin", name="registro_admin")
     * @Method({"GET", "POST"})
     */
    public function registroAdmin(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();

        $usuario = new Usuario();
        $form = $this->createForm('AppBundle\Form\UsuarioAdminType', $usuario);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $isAdmin = $request->get('isAdmin');
            if($isAdmin==true)
            {
                $usuario->setIsAdmin(1);
            }

            $usuario->setIsActive(1);
            $password = $this->container->get('security.password_encoder')->encodePassword($usuario, $usuario->getPassword());
            $usuario->setPassword($password);
            $usuario->setImagen('imagenes/anonim.png');

            $em = $this->getDoctrine()->getManager();
            $em->persist($usuario);
            $em->flush();

            $this->addFlash(
                'notice',
                'El Usuario se ha creado correctamente'
            );
            return $this->redirect($this->generateUrl('route_administrar'));

        }
        else
        {
            return $this->render('AppBundle:usuario:newAdmin.html.twig', array(
                'categorias' => $categorias,
                'usuario' => $usuario,
                'form' => $form->createView(),
            ));
        }
    }

    /**
     * Creates a form to delete a Usuario entity.
     *
     * @param Usuario $usuario The Usuario entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Usuario $usuario)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('usuario_delete', array('id' => $usuario->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
}
