<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Anuncio;
use AppBundle\Entity\Oferta;
use AppBundle\Form\AnuncioType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Anuncio controller.
 *
 * @Route("/anuncio")
 */
class AnuncioController extends Controller
{
    /**
     * Lists all Anuncio entities.
     *
     * @Route("/", name="anuncio_index")
     * @Template("AppBundle:anuncio:index.html.twig")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $anuncios = $em->getRepository('AppBundle:Anuncio')->findAll();

        return array(
            'anuncios' => $anuncios,
        );
    }

    /**
     * @Route("/new", name="anuncio_new")
     * @Template("AppBundle:anuncio:new.html.twig")
     * @Method({"GET"})
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $form = $this->createForm('AppBundle\Form\AnuncioType');

        return array(
            'categorias' => $categorias,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a new Oferta entity.
     *
     * @Route("/insertar", name="anuncio_insertar")
     * @Method({"GET", "POST"})
     */
    public function insertarAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();

        $anuncio = new Anuncio();

        $form = $this->createForm('AppBundle\Form\AnuncioType', $anuncio);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $anuncio->upload($anuncio->getImagen());
            $anuncio->upload2($anuncio->getImagen2());
            $anuncio->upload3($anuncio->getImagen3());
            $fecha= date("Y-m-d H:i:s");
            $anuncio->setFecha($fecha);
            $anuncio->setUsuario($this->getUser());
            $anuncio->setLatitud($this->get('request')->request->get('latitud'));
            $anuncio->setLongitud($this->get('request')->request->get('longitud'));
            $em->persist($anuncio);
            $em->flush();

            $this->addFlash(
                'notice',
                'El Anuncio se ha creado correctamente'
            );
            return $this->redirect($this->generateUrl('route_listar'));
        }
        else
        {
            return $this->render('AppBundle:anuncio:new.html.twig', array(
                'categorias' => $categorias,
                'anuncio' => $anuncio,
                'form' => $form->createView(),
            ));
        }

    }

    /**
     * Finds and displays a Anuncio entity.
     * @Template("AppBundle:anuncio:show.html.twig")
     * @Route("/{id}", name="anuncio_show")
     * @Method("GET")
     */
    public function showAction(Anuncio $anuncio)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $usuario=$this->getUser();
        $seguimientos = $em->getRepository('AppBundle:Seguimiento')->findByUsuarioId($usuario);

        return array(
            'anuncio' => $anuncio,
            'categorias' => $categorias,
            'seguimientos'=> $seguimientos,
        );
    }



    /**
     * Displays a form to edit an existing Anuncio entity.
     *
     * @Route("/{id}/edit", name="anuncio_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Anuncio $anuncio)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();

        $editForm = $this->createForm('AppBundle\Form\AnuncioType', $anuncio);
        $editForm->handleRequest($request);

        if ($anuncio->getUsuario()==$this->getUser() || $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            if ($editForm->isSubmitted() && $editForm->isValid())
            {
                $anuncio->upload($anuncio->getImagen());
                $anuncio->upload2($anuncio->getImagen2());
                $anuncio->upload3($anuncio->getImagen3());
                $em = $this->getDoctrine()->getManager();
                $em->persist($anuncio);
                $em->flush();
                $this->addFlash(
                    'notice',
                    'El Anuncio se ha editado correctamente'
                );
                return $this->redirectToRoute('route_listar');
            }

            return $this->render('AppBundle:anuncio:edit.html.twig', array(
                'anuncio' => $anuncio,
                'form' => $editForm->createView(),
                'categorias'=>$categorias,
            ));
        }
        else
        {
            $this->addFlash(
                'error',
                'No puedes editar el anuncio de otro usuario'
            );
            return $this->redirect($this->generateUrl('route_listar'));
        }

    }

    /**
     * Deletes a Anuncio entity.
     *
     * @Route("/{id}/delete", name="anuncio_delete")
     * @Method({"GET"})
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $anuncio = $em->getRepository('AppBundle:Anuncio')->find($id);
        $ofertas = $anuncio->getAnuncioId();

        if ($anuncio->getUsuario()==$this->getUser() ||  $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            if(count($ofertas)>0)
            {
                $this->addFlash(
                    'error',
                    'No puedes borrar el anuncio si tiene ofertas'
                );
                return $this->redirect($this->generateUrl('route_listar'));

            }
            else
            {
                $em->remove($anuncio);
                $em->flush();

                $this->addFlash(
                    'notice',
                    'El Anuncio se ha borrrado correctamente'
                );
                return $this->redirect($this->generateUrl('route_listar'));
            }
        }

        else
        {
            $this->addFlash(
                'error',
                'No puedes borrar el anuncio de otro usuario'
            );
            return $this->redirect($this->generateUrl('route_listar'));
        }

    }


    /**
     * Creates a form to delete a Anuncio entity.
     *
     * @param Anuncio $anuncio The Anuncio entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Anuncio $anuncio)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('anuncio_delete', array('id' => $anuncio->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
