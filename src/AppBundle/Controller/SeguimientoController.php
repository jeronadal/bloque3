<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Seguimiento;
use AppBundle\Form\SeguimientoType;

/**
 * Seguimiento controller.
 *
 * @Route("/seguimiento")
 */
class SeguimientoController extends Controller
{

    /**
     * Creates a new Seguimiento entity.
     *
     * @Route("/{id}/new",name="seguimiento_new")
     * @Method({"GET"})
     */
    public function newAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $anuncio = $em->getRepository('AppBundle:Anuncio')->find($id);
        $seguimiento = new Seguimiento();

        $seguimiento->setAnuncioId($anuncio);
        $seguimiento->setUsuarioId($this->getUser());
        $seguimiento->setAviso(0);

        $em->persist($seguimiento);
        $em->flush();

        return $this->redirectToRoute('route_listar');

    }

    /**
     * Deletes a Seguimiento entity.
     *
     * @Route("/{id}/delete", name="seguimiento_delete")
     * @Method({"GET"})
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $seguimiento = $em->getRepository('AppBundle:Seguimiento')->findOneByAnuncioId($id);

        $em->remove($seguimiento);
        $em->flush();
        return $this->redirect($this->generateUrl('route_listar'));
    }

    /**
     * Deletes a Seguimiento entity.
     *
     * @Route("/{id}/quitarAviso", name="seguimiento_quitarAviso")
     * @Method({"GET"})
     */
    public function quitarAviso($id)
    {
        $em = $this->getDoctrine()->getManager();
        $seguimiento = $em->getRepository('AppBundle:Seguimiento')->findOneBy(array(
            'anuncioId' => $id,
            'usuarioId' => $this->getUser(),
        ));

        $seguimiento->setAviso(0);
        $em = $this->getDoctrine()->getManager();
        $em->persist($seguimiento);
        $em->flush();

        return $this->redirect($this->generateUrl('route_homepage'));
    }

}
