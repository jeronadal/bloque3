<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Oferta;
use AppBundle\Form\OfertaType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Oferta controller.
 *
 * @Route("/anuncio/{idAnuncio}")
 */
class OfertaController extends Controller
{
    /**
     * Lists all Oferta entities.
     *
     * @Route("/ofertas", name="oferta_index")
     * @Template("AppBundle:oferta:index.html.twig")
     * @Method("GET")
     */
    public function indexAction($idAnuncio)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $anuncio = $em->getRepository('AppBundle:Anuncio')->findOneById($idAnuncio);
        $ofertas = $anuncio->getAnuncioId();

        return array(
            'anuncio' => $anuncio,
            'ofertas' => $ofertas,
            'categorias'=> $categorias,
        );
    }

    /**
     * @Route("/oferta/new", name="oferta_new")
     * @Template("AppBundle:oferta:new.html.twig")
     * @Method({"GET"})
     */
    public function newAction($idAnuncio)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $anuncio = $em->getRepository('AppBundle:Anuncio')->findOneById($idAnuncio);

        $form = $this->createForm('AppBundle\Form\OfertaType');

        return array(
            'categorias' => $categorias,
            'anuncio' => $anuncio,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a new Oferta entity.
     *
     * @Route("/oferta/insertar", name="oferta_insertar")
     * @Method({"GET", "POST"})
     */
    public function insertarAction(Request $request, $idAnuncio)
    {
        $em = $this->getDoctrine()->getManager();
        $categorias = $em->getRepository('AppBundle:Categoria')->findAll();
        $anuncio = $em->getRepository('AppBundle:Anuncio')->findOneById($idAnuncio);
        $seguimiento = $em->getRepository('AppBundle:Seguimiento')->findByAnuncioId($idAnuncio);

        $ofertum = new Oferta();
        $form = $this->createForm('AppBundle\Form\OfertaType', $ofertum);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $fecha= date("Y-m-d H:i:s");
            $ofertum->setFecha($fecha);
            $ofertum->setOferta($anuncio);
            $ofertum->setUsuario($this->getUser());
            foreach($seguimiento as $s)
            {
                $s->setAviso(1);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($ofertum);
            $em->flush();

            return $this->redirect($this->generateUrl('route_homepage'));
        }
        else
        {
            return $this->render('AppBundle:oferta:new.html.twig', array(
                'categorias' => $categorias,
                'anuncio' => $anuncio,
                'oferta' => $ofertum,
                'form' => $form->createView(),
            ));
        }

    }

    /**
     * Deletes a Oferta entity.
     *
     * @Route("/oferta/{id}/delete", name="oferta_delete")
     * @Method({"GET"})
     */
    public function deleteAction($idAnuncio, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $anuncio = $em->getRepository('AppBundle:Anuncio')->findOneById($idAnuncio);
        $oferta = $em->getRepository('AppBundle:Oferta')->findOneById($id);

        if ($anuncio->getUsuario()==$this->getUser() ||  $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            $em->remove($oferta);
            $em->flush();
            $this->addFlash(
                'notice',
                'Oferta borrada correctamente'
            );
            return $this->redirect($this->generateUrl('route_listar'));
        }

        if($anuncio->getUsuario()!==$this->getUser())
        {
             $this->addFlash(
                'error',
                'No puedes borrar la oferta de otro usuario'
            );
            return $this->redirect($this->generateUrl('route_listar'));
        }
    }

    /**
     * Displays a form to edit an existing Oferta entity.
     *
     * @Route("/{id}/edit", name="oferta_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Oferta $ofertum)
    {
        $deleteForm = $this->createDeleteForm($ofertum);
        $editForm = $this->createForm('AppBundle\Form\OfertaType', $ofertum);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ofertum);
            $em->flush();

            return $this->redirectToRoute('oferta_edit', array('id' => $ofertum->getId()));
        }

        return $this->render('oferta/edit.html.twig', array(
            'ofertum' => $ofertum,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a form to delete a Oferta entity.
     *
     * @param Oferta $ofertum The Oferta entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Oferta $ofertum)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('oferta_delete', array('id' => $ofertum->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
