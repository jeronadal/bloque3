<?php

namespace AppBundle\Controller\Rest;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Routing\ClassResourceInterface;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use AppBundle\Entity\Oferta;
use AppBundle\Entity\Usuario;
use AppBundle\Entity\Anuncio;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\RecursiveValidator;

/**
 * @RouteResource("Oferta")
 */
class OfertaRestController extends Controller implements ClassResourceInterface
{


    public function cgetAction()
    {
        $em = $this->getDoctrine()->getManager();
        $ofertas = $em->getRepository('AppBundle:Oferta')->findAll();

        return array(
            'ofertas'=> $ofertas,
        );
    }

    public function getAction(Oferta $oferta)
    {
        return array(
            'oferta'=> $oferta,
        );

    }

    public function postFechaAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $fecha1 = $request->request->get('fecha1');
        $fecha2 = $request->request->get('fecha2');

        $qb = $em->createQueryBuilder();

        $qb->select('ofe')
            ->from('AppBundle:Oferta', 'ofe');
        $qb->where('ofe.fecha between :fecha1 and :fecha2')
            ->setParameter('fecha1',$fecha1.'%')
            ->setParameter('fecha2',$fecha2.'%');


        $ofertas= $qb->getQuery()->getResult();

        return array(
            'ofertas'=> $ofertas,
        );

    }

    public function postAction(Request $request, $idAnuncio)
    {
        $em = $this->getDoctrine()->getManager();
        $anuncio = $em->getRepository('AppBundle:Anuncio')->findOneById($idAnuncio);
        $seguimiento = $em->getRepository('AppBundle:Seguimiento')->findByAnuncioId($idAnuncio);
        $usuario = $em->getRepository('AppBundle:Usuario')->findOneById($anuncio->getUsuario());

        $titulo = $request->request->get('titulo');
        $texto = $request->request->get('texto');
        $importe = $request->request->get('importe');

        if (!isset($titulo) || $titulo === '')
            return array('code' => 500, 'message' => 'Es obligatorio pasar un título');
        if (!isset($texto) || $texto === '')
            return array('code' => 500, 'message' => 'Es obligatorio pasar un texto');
        if (!isset($importe) || $importe === '')
            return array('code' => 500, 'message' => 'Es obligatorio pasar un importe');

        $oferta = new Oferta();
        $oferta->setTitulo($titulo);
        $oferta->setTexto($texto);
        $oferta->setImporte($importe);
        $fecha= date("Y-m-d H:i:s");
        $oferta->setFecha($fecha);
        $oferta->setOferta($anuncio);
        $oferta->setUsuario($usuario);

        foreach($seguimiento as $s)
        {
            $s->setAviso(1);
        }

        $em->persist($oferta);
        $em->flush();

        return array('code' => 200, 'message' => 'Oferta añadida correctamente');
    }

    public function putAction(Request $request, Oferta $oferta)
    {
        $em = $this->getDoctrine()->getManager();

        $titulo = $request->request->get('titulo');
        $texto = $request->request->get('texto');
        $importe = $request->request->get('importe');

        $oferta->setTitulo($titulo);
        $oferta->setTexto($texto);
        $oferta->setImporte($importe);

        $em->persist($oferta);
        $em->flush();

        return array('code' => 200, 'message' => 'Oferta editada correctamente');
    }


    public function deleteAction($idOferta)
    {
        $em = $this->getDoctrine()->getManager();
        $oferta = $em->getRepository('AppBundle:Oferta')->findOneById($idOferta);

        $em->remove($oferta);
        $em->flush();

        return array('code' => 200, 'message' => 'Oferta eliminada correctamente');
    }
}