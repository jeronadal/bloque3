<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Seguimiento
 *
 * @ORM\Table(name="seguimiento")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SeguimientoRepository")
 */
class Seguimiento
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Anuncio", inversedBy="seguimiento")
     * @ORM\JoinColumn(name="anuncio_id", referencedColumnName="id", nullable=false)
     */
    private $anuncioId;



    /**
     * @var int
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="seguimiento")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", nullable=false)
     */

    private $usuarioId;

    /**
     * @ORM\Column(type="boolean")
     */
    private $aviso;

    /**
     * @return mixed
     */
    public function getAviso()
    {
        return $this->aviso;
    }

    /**
     * @param mixed $aviso
     */
    public function setAviso($aviso)
    {
        $this->aviso = $aviso;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set anuncioId
     *
     * @param integer $anuncioId
     * @return Seguimiento
     */
    public function setAnuncioId($anuncioId)
    {
        $this->anuncioId = $anuncioId;

        return $this;
    }

    /**
     * Get anuncioId
     *
     * @return integer 
     */
    public function getAnuncioId()
    {
        return $this->anuncioId;
    }

    /**
     * Set usuarioId
     *
     * @param integer $usuarioId
     * @return Seguimiento
     */
    public function setUsuarioId($usuarioId)
    {
        $this->usuarioId = $usuarioId;

        return $this;
    }

    /**
     * Get usuarioId
     *
     * @return integer 
     */
    public function getUsuarioId()
    {
        return $this->usuarioId;
    }
}
