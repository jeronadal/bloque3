<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Oferta
 *
 * @ORM\Table(name="oferta")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OfertaRepository")
 */
class Oferta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="ofertas")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", nullable=false)
     */
    private $usuario;

    /**
     * @ORM\ManyToOne(targetEntity="Anuncio", inversedBy="anuncio_id")
     * @ORM\JoinColumn(name="anuncio_id", referencedColumnName="id", nullable=false)
     */
    private $oferta;

    /**
     * @var string
     *
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     *
     * @ORM\Column(name="texto", type="string", length=255)
     */
    private $texto;

    /**
     * @var float
     *
     * @ORM\Column(name="importe", type="float")
     */
    private $importe;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Oferta
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set texto
     *
     * @param string $texto
     *
     * @return Oferta
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }

    /**
     * Set importe
     *
     * @param float $importe
     *
     * @return Oferta
     */
    public function setImporte($importe)
    {
        $this->importe = $importe;

        return $this;
    }

    /**
     * Get importe
     *
     * @return string
     */
    public function getImporte()
    {
        return $this->importe;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->anuncio_id = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     *
     * @return Oferta
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Add anuncioId
     *
     * @param \AppBundle\Entity\Anuncio $anuncioId
     *
     * @return Oferta
     */
    public function addAnuncioId(\AppBundle\Entity\Anuncio $anuncioId)
    {
        $this->anuncio_id[] = $anuncioId;

        return $this;
    }

    /**
     * Remove anuncioId
     *
     * @param \AppBundle\Entity\Anuncio $anuncioId
     */
    public function removeAnuncioId(\AppBundle\Entity\Anuncio $anuncioId)
    {
        $this->anuncio_id->removeElement($anuncioId);
    }

    /**
     * Get anuncioId
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnuncioId()
    {
        return $this->anuncio_id;
    }

    /**
     * Set oferta
     *
     * @param \AppBundle\Entity\Anuncio $oferta
     *
     * @return Oferta
     */
    public function setOferta(\AppBundle\Entity\Anuncio $oferta)
    {
        $this->oferta = $oferta;

        return $this;
    }

    /**
     * Get oferta
     *
     * @return \AppBundle\Entity\Anuncio
     */
    public function getOferta()
    {
        return $this->oferta;
    }
}
