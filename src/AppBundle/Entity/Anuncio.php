<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Anuncio
 *
 * @ORM\Table(name="anuncio")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnuncioRepository")
 */
class Anuncio
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fecha", type="datetime")
     */
    private $fecha;

    /**
     * @var string
     * @Assert\NotBlank(message="El campo titulo no puede quedarse vacío")
     * @ORM\Column(name="titulo", type="string", length=255)
     */
    private $titulo;

    /**
     * @var string
     * @Assert\NotBlank(message="El campo texto no puede quedarse vacío")
     * @ORM\Column(name="texto", type="string", length=255)
     */
    private $texto;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255, nullable=true)
     * @Assert\File( maxSize = "1024k", mimeTypesMessage = "Please upload a valid Image")
     */
    private $imagen;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen2", type="string", length=255, nullable=true)
     * @Assert\File( maxSize = "1024k", mimeTypesMessage = "Please upload a valid Image")
     */
    private $imagen2;

    /**
     * @ORM\ManyToOne(targetEntity="Categoria", inversedBy="anuncios")
     * @ORM\JoinColumn(name="categoria_id", referencedColumnName="id", nullable=false)
     */

    private $categoria;

    /**
     * @ORM\ManyToOne(targetEntity="Usuario", inversedBy="anuncios")
     * @ORM\JoinColumn(name="usuario_id", referencedColumnName="id", nullable=false)
     */

    private $usuario;

    /**
     * @ORM\OneToMany(targetEntity="Oferta", mappedBy="oferta")
     */
    private $anuncio_id;

    /**
     * @var string
     *
     * @ORM\Column(name="imagen3", type="string", length=255, nullable=true)
     * @Assert\File( maxSize = "1024k", mimeTypesMessage = "Please upload a valid Image")
     */
    private $imagen3;

    /**
     * @var float
     * @Assert\NotBlank(message="El campo texto no puede quedarse vacío")
     * @ORM\Column(name="precio", type="float")
     */
    private $precio;

    /**
     * @var float
     *
     * @ORM\Column(name="latitud", type="float")
     */
    private $latitud;

    /**
     * @var float
     *
     * @ORM\Column(name="longitud", type="float")
     */
    private $longitud;

    /**
     * @ORM\OneToMany(targetEntity="Seguimiento", mappedBy="anuncioId")
     */
    private $seguimiento;

    /**
     * @return mixed
     */
    public function getSeguimiento()
    {
        return $this->seguimiento;
    }

    /**
     * @param mixed $seguimiento
     */
    public function setSeguimiento($seguimiento)
    {
        $this->seguimiento = $seguimiento;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fecha
     *
     * @param \DateTime $fecha
     *
     * @return Anuncio
     */
    public function setFecha($fecha)
    {
        $this->fecha = new \DateTime("now");
    }

    /**
     * Get fecha
     *
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set titulo
     *
     * @param string $titulo
     *
     * @return Anuncio
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }

    /**
     * Get titulo
     *
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * Set texto
     *
     * @param string $texto
     *
     * @return Anuncio
     */
    public function setTexto($texto)
    {
        $this->texto = $texto;

        return $this;
    }

    /**
     * Get texto
     *
     * @return string
     */
    public function getTexto()
    {
        return $this->texto;
    }


    /**
     * Set imagen
     *
     * @param string $imagen
     *
     * @return Anuncio
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;

        return $this;
    }

    public function upload()
    {
        if (null === $this->imagen)
        {return;}
        $file = $this->getImagen();
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move(
            $this->getUploadRootDir(),
            $fileName
        );
        $this->setImagen("subidas/".$fileName);
    }

    public function upload2()
    {
        if (null === $this->imagen2)
        {return;}
        $file = $this->getImagen2();
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move(
            $this->getUploadRootDir(),
            $fileName
        );
        $this->setImagen2("subidas/".$fileName);
    }

    public function upload3()
    {
        if (null === $this->imagen3)
        {return;}
        $file = $this->getImagen3();
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move(
            $this->getUploadRootDir(),
            $fileName
        );
        $this->setImagen3("subidas/".$fileName);
    }


    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'subidas';
    }

    /**
     * Get imagen
     *
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * Set imagen2
     *
     * @param string $imagen2
     *
     * @return Anuncio
     */
    public function setImagen2($imagen2)
    {
        $this->imagen2 = $imagen2;

        return $this;
    }

    /**
     * Get imagen2
     *
     * @return string
     */
    public function getImagen2()
    {
        return $this->imagen2;
    }

    /**
     * Set categoria
     *
     * @param string $categoria
     *
     * @return Anuncio
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;

        return $this;
    }

    /**
     * Get categoria
     *
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * Set precio
     *
     * @param float $precio
     *
     * @return Anuncio
     */
    public function setPrecio($precio)
    {
        $this->precio = $precio;

        return $this;
    }

    /**
     * Get precio
     *
     * @return float
     */
    public function getPrecio()
    {
        return $this->precio;
    }

    /**
     * Set imagen3
     *
     * @param string $imagen3
     *
     * @return Anuncio
     */
    public function setImagen3($imagen3)
    {
        $this->imagen3 = $imagen3;

        return $this;
    }

    /**
     * Get imagen3
     *
     * @return string
     */
    public function getImagen3()
    {
        return $this->imagen3;
    }

    /**
     * Set usuario
     *
     * @param \AppBundle\Entity\Usuario $usuario
     *
     * @return Anuncio
     */
    public function setUsuario(\AppBundle\Entity\Usuario $usuario)
    {
        $this->usuario = $usuario;

        return $this;
    }

    /**
     * Get usuario
     *
     * @return \AppBundle\Entity\Usuario
     */
    public function getUsuario()
    {
        return $this->usuario;
    }

    /**
     * Set oferta
     *
     * @param \AppBundle\Entity\Oferta $oferta
     *
     * @return Anuncio
     */
    public function setOferta(\AppBundle\Entity\Oferta $oferta)
    {
        $this->oferta = $oferta;

        return $this;
    }

    /**
     * Get oferta
     *
     * @return \AppBundle\Entity\Oferta
     */
    public function getOferta()
    {
        return $this->oferta;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->oferta = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add anuncioId
     *
     * @param \AppBundle\Entity\Oferta $anuncioId
     *
     * @return Anuncio
     */
    public function addAnuncioId(\AppBundle\Entity\Oferta $anuncioId)
    {
        $this->anuncio_id = $anuncioId;

        return $this;
    }

    /**
     * Remove anuncioId
     *
     * @param \AppBundle\Entity\Oferta $anuncioId
     */
    public function removeAnuncioId(\AppBundle\Entity\Oferta $anuncioId)
    {
        $this->anuncio_id->removeElement($anuncioId);
    }

    /**
     * Get anuncioId
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnuncioId()
    {
        return $this->anuncio_id;
    }

    /**
     * Set latitud
     *
     * @param float $latitud
     * @return Anuncio
     */
    public function setLatitud($latitud)
    {
        $this->latitud = $latitud;

        return $this;
    }

    /**
     * Get latitud
     *
     * @return float 
     */
    public function getLatitud()
    {
        return $this->latitud;
    }

    /**
     * Set longitud
     *
     * @param float $longitud
     * @return Anuncio
     */
    public function setLongitud($longitud)
    {
        $this->longitud = $longitud;

        return $this;
    }

    /**
     * Get longitud
     *
     * @return float 
     */
    public function getLongitud()
    {
        return $this->longitud;
    }

}
