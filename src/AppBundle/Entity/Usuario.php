<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;


/**
 * Usuario
 *
 * @ORM\Table(name="usuario")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UsuarioRepository")
 * @UniqueEntity("nick", message="Ya existe un usuario con ese nick")
 * @UniqueEntity("email", message="Ya existe un usuario con ese email")
 */
class Usuario implements AdvancedUserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Anuncio", mappedBy="usuario")
     */
    private $anuncios;

    /**
     * @ORM\OneToMany(targetEntity="Oferta", mappedBy="usuario")
     */
    private $ofertas;

    /**
     * @var string
     *
     * @ORM\Column(name="nombre", type="string", length=255)
     * @Assert\NotBlank(message="El campo nombre no puede quedarse vacío")
     * @ORM\Column(name="salt", type="string", length=255)
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "longitud máxima {{ limit }}")
     */
    private $nombre;

    /**
     * @var string
     */
    private $salt;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     * @Assert\Email(
     *      message = "La direccion '{{ value }}' no es valida.")
     */
    private $email;

    /**
     * @var string
     * @Assert\NotBlank(message="El campo nick no puede quedarse vacío")
     * @ORM\Column(name="nick", type="string", length=255, unique=true)
     * @Assert\Length(
     *      min = 4,
     *      max = 12,
     *      minMessage = "longitud mínima {{ limit }}")
     *      maxMessage = "longitud máxima {{ limit }}")
     * @Assert\Regex(
     *      pattern="/^[\w-]+$/",
     *      message="nick: sólo caracteres alfanuméricos y guiones")
     */
    private $nick;

    /**
     * @var string
     * @Assert\NotBlank(message="El campo password no puede quedarse vacío")
     * @ORM\Column(name="password", type="string", length=255)
     * @Assert\Length(
     *      min = 6,
     *      max = 10,
     *      minMessage = "longitud mínima {{ limit }}")
     *      maxMessage = "longitud máxima {{ limit }}")
     *      groups = {"Default"}
     * @Assert\Regex(
     *      pattern="/^[\w-]+$/",
     *      message="password: sólo caracteres alfanuméricos y guiones")
     */
    private $password;


    /**
     * @var string
     *
     * @ORM\Column(name="imagen", type="string", length=255, nullable=true)
     * @Assert\File( maxSize = "1024k", mimeTypesMessage = "Please upload a valid Image")
     */
    private $imagen;

    /**
     * @var string
     *
     * @ORM\Column(name="telefono", type="string", length=255, nullable=true)
     */
    private $telefono;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_admin", type="boolean")
     */
    private $isAdmin;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fechaNacimiento", type="date", nullable=true)
     */
    private $fechaNacimiento;

    /**
     * @var string
     *
     * @ORM\Column(name="domicilio", type="string", length=255, nullable=true)
     */
    private $domicilio;

    /**
     * @var int
     *
     * @ORM\Column(name="codigoPostal", type="integer", length=255)
     *  @Assert\NotBlank(message="El campo codigo Postal no puede quedarse vacío")
     */
    private $codigoPostal;

    /**
     * @var string
     * @ORM\Column(name="municipio", type="string", length=255, nullable=true)
     */
    private $municipio;

    /**
     * @var string
     *
     * @ORM\Column(name="provincia", type="string", length=255, nullable=true)
     */
    private $provincia;

    /**
     * @var string
     *
     * @ORM\Column(name="web", type="string", length=255, nullable=true)
     *
     */
    private $web;

    /**
     * @var string
     *
     * @ORM\Column(name="experiencia", type="float", nullable=true)
     */
    private $experiencia;

    /**
     * @var string
     * @Assert\NotBlank(message="El campo Dni no puede quedarse vacío")
     * @ORM\Column(name="dni", type="string", length=255, unique=true)
     */
    private $dni;

    /**
     * @var bool
     *
     * @ORM\Column(name="isActive", type="boolean")
     * @Assert\Type(type="bool", message="El valor {{value}} => {{type}}.")
     */
    private $isActive;

    /**
     * @return mixed
     */
    public function getSeguimiento()
    {
        return $this->seguimiento;
    }

    /**
     * @param mixed $seguimiento
     */
    public function setSeguimiento($seguimiento)
    {
        $this->seguimiento = $seguimiento;
    }

    /**
     * @ORM\OneToMany(targetEntity="Seguimiento", mappedBy="usuarioId")
     */
    private $seguimiento;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nombre
     *
     * @param string $nombre
     *
     * @return Usuario
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }

    /**
     * @return string
     */
    public function getImagen()
    {
        return $this->imagen;
    }

    /**
     * @param string $imagen
     */
    public function setImagen($imagen)
    {
        $this->imagen = $imagen;
    }


    /**
     * Get nombre
     *
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Usuario
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set nick
     *
     * @param string $nick
     *
     * @return Usuario
     */
    public function setNick($nick)
    {
        $this->nick = $nick;

        return $this;
    }

    /**
     * Get nick
     *
     * @return string
     */
    public function getNick()
    {
        return $this->nick;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Usuario
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     *
     * @return Usuario
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set isAdmin
     *
     * @param boolean $isAdmin
     *
     * @return Usuario
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;

        return $this;
    }

    /**
     * Get isAdmin
     *
     * @return bool
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * Set fechaNacimiento
     *
     * @param \DateTime $fechaNacimiento
     *
     * @return Usuario
     */
    public function setFechaNacimiento($fechaNacimiento)
    {
        $this->fechaNacimiento = $fechaNacimiento;

        return $this;
    }

    /**
     * Get fechaNacimiento
     *
     * @return \DateTime
     */
    public function getFechaNacimiento()
    {
        return $this->fechaNacimiento;
    }

    /**
     * Set domicilio
     *
     * @param string $domicilio
     *
     * @return Usuario
     */
    public function setDomicilio($domicilio)
    {
        $this->domicilio = $domicilio;

        return $this;
    }

    /**
     * Get domicilio
     *
     * @return string
     */
    public function getDomicilio()
    {
        return $this->domicilio;
    }

    /**
     * Set codigoPostal
     *
     * @param string $codigoPostal
     *
     * @return Usuario
     */
    public function setCodigoPostal($codigoPostal)
    {
        $this->codigoPostal = $codigoPostal;

        return $this;
    }

    /**
     * Get codigoPostal
     *
     * @return string
     */
    public function getCodigoPostal()
    {
        return $this->codigoPostal;
    }

    /**
     * Set municipio
     *
     * @param string $municipio
     *
     * @return Usuario
     */
    public function setMunicipio($municipio)
    {
        $this->municipio = $municipio;

        return $this;
    }

    /**
     * Get municipio
     *
     * @return string
     */
    public function getMunicipio()
    {
        return $this->municipio;
    }

    /**
     * Set provincia
     *
     * @param string $provincia
     *
     * @return Usuario
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return string
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set web
     *
     * @param string $web
     *
     * @return Usuario
     */
    public function setWeb($web)
    {
        $this->web = $web;

        return $this;
    }

    /**
     * Get web
     *
     * @return string
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Set experiencia
     *
     * @param string $experiencia
     *
     * @return Usuario
     */
    public function setExperiencia($experiencia)
    {
        $this->experiencia = $experiencia;

        return $this;
    }

    /**
     * Get experiencia
     *
     * @return float
     */
    public function getExperiencia()
    {
        return $this->experiencia;
    }

    /**
     * Set dni
     *
     * @param string $dni
     *
     * @return Usuario
     */
    public function setDni($dni)
    {
        $this->dni = $dni;

        return $this;
    }

    /**
     * Get dni
     *
     * @return string
     */
    public function getDni()
    {
        return $this->dni;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->anuncios = new \Doctrine\Common\Collections\ArrayCollection();
        $this->ofertas = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add anuncio
     *
     * @param \AppBundle\Entity\Anuncio $anuncio
     *
     * @return Usuario
     */
    public function addAnuncio(\AppBundle\Entity\Anuncio $anuncio)
    {
        $this->anuncios[] = $anuncio;

        return $this;
    }

    /**
     * Remove anuncio
     *
     * @param \AppBundle\Entity\Anuncio $anuncio
     */
    public function removeAnuncio(\AppBundle\Entity\Anuncio $anuncio)
    {
        $this->anuncios->removeElement($anuncio);
    }

    /**
     * Get anuncios
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnuncios()
    {
        return $this->anuncios;
    }

    /**
     * Add oferta
     *
     * @param \AppBundle\Entity\Anuncio $oferta
     *
     * @return Usuario
     */
    public function addOferta(\AppBundle\Entity\Anuncio $oferta)
    {
        $this->ofertas[] = $oferta;

        return $this;
    }

    /**
     * Remove oferta
     *
     * @param \AppBundle\Entity\Anuncio $oferta
     */
    public function removeOferta(\AppBundle\Entity\Anuncio $oferta)
    {
        $this->ofertas->removeElement($oferta);
    }

    /**
     * Get ofertas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOfertas()
    {
        return $this->ofertas;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return Usuario
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return bool
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set tokenRegistro
     *
     * @param string $tokenRegistro
     *
     * @return Usuario
     */
    public function setTokenRegistro($tokenRegistro)
    {
        $this->tokenRegistro = $tokenRegistro;

        return $this;
    }

    /**
     * Get tokenRegistro
     *
     * @return string
     */
    public function getTokenRegistro()
    {
        return $this->tokenRegistro;
    }

    public function upload()
    {
        if (null === $this->getImagen())
        {return;}
        $file = $this->getImagen();
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $file->move(
            $this->getUploadRootDir(),
            $fileName
        );
        $this->setImagen("subidas/".$fileName);
    }


    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    protected function getUploadDir()
    {
        return 'subidas';
    }

    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        if ($this->isAdmin)
            return array('ROLE_ADMIN');

        return array('ROLE_USER');
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->getIsActive();
    }
    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return Usuario
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }


    public function getUsername()
    {
        // TODO: Implement getUsername() method.
    }

}
