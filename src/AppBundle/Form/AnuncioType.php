<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class AnuncioType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titulo')
            ->add('texto')
            ->add('imagen')
            ->add('imagen2')
            ->add('imagen3')
            ->add('precio')
            ->add('categoria', EntityType::class, array(
                'class' => 'AppBundle:Categoria',
                'choice_label' => 'nombre'))
            ->add('imagen', FileType::class, array(
                'data_class' => null
            ))
            ->add('imagen2', FileType::class, array(
                'data_class' => null
            ))
            ->add('imagen3', FileType::class, array(
            'data_class' => null
            ));
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Anuncio'
        ));
    }
}
