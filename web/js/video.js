var video = document.getElementsByTagName('video')[0];
var btn_reiniciar = document.getElementsByTagName('button')[1];
var btn_atrasar = document.getElementsByTagName('button')[2];
var btn_play = document.getElementsByTagName('button')[3];
var btn_adelantar = document.getElementsByTagName('button')[4];
var btn_silenciar = document.getElementsByTagName('button')[5];
var btn_volumenmas = document.getElementsByTagName('button')[6];
var btn_volumenmenos = document.getElementsByTagName('button')[7];
var volumen = document.getElementById('volumen');
var barraVideo = document.getElementsByTagName('progress')[0];
var vol = video.volume;

function playPuase()
{
    if(video.paused)
    {
        video.play();
        btn_play.innerHTML=("Puase");
    }
    else
    {
        video.pause();
        btn_play.innerHTML=("Reproducir");
    }
}

function reiniciar()
{
    video.load();
    video.play();
}

function silenciar()
{
    if(video.muted==false)
    {
        video.muted = true;
        btn_silenciar.innerHTML=("Escuchar");
    }
    else
    {
        video.muted=false;
        btn_silenciar.innerHTML=("Silenciar");
    }
}

function setVol(value)
{
    vol += value;

    if (vol >= 0 && vol <= 1)
    {
        video.volume = vol;
        volumen.innerHTML=("Volumen " + vol.toFixed(1)*100+ "%");
    }
}

function adelantar()
{
    video.currentTime+=5;
}
function atrasar()
{
    video.currentTime-=5;
}


function barra()
{
    var porcentaje = Math.floor((100 / video.duration) * video.currentTime);
    barraVideo.value = porcentaje;
}



btn_play.addEventListener('click', playPuase, false);
btn_play.addEventListener('click', playPuase, false);
btn_reiniciar.addEventListener('click', reiniciar, false);
btn_silenciar.addEventListener('click', silenciar, false);
btn_volumenmas.addEventListener('click', function () { setVol(+.1);}, false);
btn_volumenmenos.addEventListener('click',  function () {setVol(-.1);}, false);
btn_adelantar.addEventListener('click', adelantar, false);
btn_atrasar.addEventListener('click', atrasar, false);
video.addEventListener('timeupdate', barra, false);

window.onload = function()
{
    setVol(null);
};
